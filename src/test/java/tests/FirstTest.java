package tests;

import org.aeonbits.owner.ConfigFactory;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.ritual.at.utils.Configurations;

public class FirstTest extends BaseTest {

    Configurations conf = ConfigFactory.create(Configurations.class, System.getProperties());

    @Test
    @DisplayName("Авторизоваться в системе")
    public void autorizationInMainPage(){
        page.navigate(conf.getMainURL());

    }

}
