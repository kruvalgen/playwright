package ru.ritual.at.utils;

import org.aeonbits.owner.Config;


@Config.LoadPolicy(Config.LoadType.MERGE)
@Config.Sources({
        "classpath:config/credentials.properties"
})
public interface Configurations extends Config{

    @Key("login")
    @DefaultValue("")
    String getLogin();

    @Key("password")
    @DefaultValue("")
    String getPassword();

    @Key("mainURL")
    @DefaultValue("")
    String getMainURL();
}
